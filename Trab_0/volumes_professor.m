function V = volumes_professor(T, A)
    n = max(T(:));
    V = zeros(n, 1);
    A_ = [A; A; A];
    for i=1:length(T)
        V(T(1,i)) = A_(1, i) + V(T(1,i));
        V(T(2,i)) = A_(2, i) + V(T(2,i));
        V(T(3,i)) = A_(3, i) + V(T(3,i));
    end
end
