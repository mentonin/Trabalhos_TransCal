%{
Copyright 2018 Luiz Georg

This file is part of Trabalhos_TransCal.

    Trabalhos_TransCal is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Trabalhos_TransCal is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trabalhos_TransCal.  If not, see <https://www.gnu.org/licenses/>.
%}

clear;clc;close; % Limpadores
pkg load fpl;    % OCTAVE biblioteca com pdemesh

%n = 100; % Numero de pontos na circunferência interna
%r_in = 1.0; % Raio da circunferencia interna
%r_ex = 2.0; % Raio da circunferencia externa
n = input('Numero de divisoes na circunferencia interna: ');
r_in = input('Raio interno: ');
r_ex = input('Raio externo: ');


% Pontos na circunferencia interna
P_theta_in = linspace(0, 2*pi, n+1);                % theta
P_theta_in = P_theta_in(1:n);                       % 2pi = 0
P_rho_in = linspace(r_in, r_in, n);                 % rho
[P_x_in, P_y_in] = pol2cart(P_theta_in, P_rho_in);  % cartesiano

% Pontos na circunferencia externa
theta_d = pi/n*0;
P_theta_ex = theta_d + linspace(0, 2*pi, n+1);      % theta
P_theta_ex = P_theta_ex(1:n);                       % 2pi = 0
P_rho_ex = linspace(r_ex, r_ex, n);                 % rho
[P_x_ex, P_y_ex] = pol2cart(P_theta_ex, P_rho_ex);  % cartesiano

P = [P_x_in, P_x_ex
     P_y_in, P_y_ex];                   % Matriz completa de pontos
P_v_in = 1:n;                           % Vetor auxiliar interno
P_v_ex = ((n+1):(n+length(P_x_ex)));    % Vetor auxiliar externo

E_in = make_E(P_v_in);  % Edges na circunferencia interna
E_ex = make_E(P_v_ex);  % Edges na circunferencia externa
E = [E_in, E_ex];       % Matriz completa de edges

% Triangulos virados para dentro
T_in = [circshift(P_v_in, -1, 2);
        P_v_in;
        P_v_ex];
% Triangulos virados para fora
T_ex = [P_v_in;
        circshift(P_v_ex, 1, 2);
        P_v_ex];

T = [T_in, T_ex];

graphics_toolkit ('fltk')       % OCTAVE seleciona o renderer
pdemesh(P,E,T)                  % Cria e plota a mesh
hold on;                        % Permite sobrepor graficos
r_in = 1;                       % Raio interno
r_ex = 2;                       % Raio interno
plot_circle(0, 0, 0);           % Cicla as cores
plot_circle(r_in, 0, 0);        % Circulo menor
plot_circle(r_ex, 0, 0);        % Circulo maior
view (2)                        % OCTAVE seleciona a vista superior
hold off;                       % Para de sobrepor graficos
axis equal                      % Iguala as escalas dos eixos
Aa = sum(area_from_T(T, P));    % Area calculada pela mesh
Ac = pi*(r_ex^2 - r_in^2);      % Area utilizando o valor de pi do programa
error_val = 100 * (Aa/Ac - 1);  % Erro porcentual
error_sca = int8(-log10(abs(error_val)) + 2);

% Exibe os resultados
fprintf('Area da mesh:       % .16f\n', Aa);
fprintf('Area esperada:      % .16f\n', Ac);
fprintf('Erro bruto:         % .6e\n', Aa - Ac);
fprintf('Erro porcentual:    % .*f%%\n', error_sca, error_val);

waitfor(figure(gcf))            % Espera que a figura seja fechada
