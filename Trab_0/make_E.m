%{
Copyright 2018 Luiz Georg

This file is part of Trabalhos_TransCal.

    Trabalhos_TransCal is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Trabalhos_TransCal is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trabalhos_TransCal.  If not, see <https://www.gnu.org/licenses/>.
%}

%% Faz Edges a partir de uma sequencia de pontos
% v e um vetor de pontos ordenados
% E e o resultado da ligação dos pontos, em ordem, e do ponto final ao inicial

function E = make_E(v)
    if (isvector(v))    % Recebe apenas vetores
        k = length(v);
        E = [v; circshift(v, -1, 1)];
    else
        error('Input must be a vector')
    end
end