%{
Copyright 2018 Luiz Georg

This file is part of Trabalhos_TransCal.

    Trabalhos_TransCal is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Trabalhos_TransCal is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trabalhos_TransCal.  If not, see <https://www.gnu.org/licenses/>.
%}

clear;clc;close; % Limpadores

pkg load fpl;    % OCTAVE biblioteca com pdemesh

% Pontos dados
P = [-2.0000, +0.0000
     -1.0000, +0.0000
     +0.7071, +0.7071
     +1.4142, +1.4142
     +0.0000, +2.0000
     -1.4142, +1.4142
     +0.0000, -1.0000
     +1.4142, -1.4142
     +0.7071, -0.7071
     +0.0000, -2.0000
     -1.4142, -1.4142
     -0.7071, +0.7071
     -0.7071, -0.7071
     +0.0000, +1.0000
     +1.0000, +0.0000
     +2.0000, +0.0000]';

% Contornos
E = [make_E([2, 13, 7, 9, 15, 3, 14, 12]), make_E([1, 11, 10, 8, 16, 4, 5, 6])];

% Triangulos
T = [8, 16, 9;
     1, 2, 12;
     3, 5, 14;
     10, 13, 11;
     3, 15, 16;
     1, 12, 6;
     7, 10, 9;
     1, 11, 13;
     5, 6, 12;
     3, 4, 5;
     3, 16, 4;
     1, 13, 2;
     7, 13, 10;
     9, 16, 15;
     5, 12, 14;
     8, 9, 10]';

graphics_toolkit ('fltk')       % OCTAVE seleciona o renderer
pdemesh(P,E,T)                  % Cria e plota a mesh
hold on;                        % Permite sobrepor graficos
r_in = 1;                       % Raio interno
r_ex = 2;                       % Raio externo
plot_circle(0, 0, 0);           % Cicla as cores
plot_circle(r_in, 0, 0);        % Circulo menor
plot_circle(r_ex, 0, 0);        % Circulo maior
view (2)                        % OCTAVE seleciona a vista superior
hold off;                       % Para de sobrepor graficos
axis equal                      % Iguala as escalas dos eixos
Aa = sum(area_from_T(T, P));    % Area calculada pela mesh
Ac = pi*(r_ex^2 - r_in^2);      % Area utilizando o valor de pi do programa
error_val = 100 * (Aa/Ac - 1);  % Erro porcentual
error_sca = int8(-log10(abs(error_val)) + 2);

% Exibe os resultados
fprintf('Area da mesh:       % .16f\n', Aa);
fprintf('Area esperada:      % .16f\n', Ac);
fprintf('Erro bruto:         % .6e\n', Aa - Ac);
fprintf('Erro porcentual:    % .*f%%\n', error_sca, error_val);

waitfor(figure(gcf))            % Espera que a figura seja fechada
