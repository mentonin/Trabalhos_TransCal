%{
Copyright 2018 Luiz Georg

This file is part of Trabalhos_TransCal.

    Trabalhos_TransCal is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Trabalhos_TransCal is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trabalhos_TransCal.  If not, see <https://www.gnu.org/licenses/>.
%}

%% Plot de um circulo com raio e centro determinados
% x = abcissa do centro
% y = ordenada do centro
% r = raio do circulo
function c = plot_circle(r, x, y)
    theta = linspace(0, 2*pi, 100);
    xc = r*cos(theta);
    yc = r*sin(theta);
    c = plot(x+xc, y+yc);
end
