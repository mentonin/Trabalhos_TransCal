%{
Copyright 2018 Luiz Georg

This file is part of Trabalhos_TransCal.

    Trabalhos_TransCal is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Trabalhos_TransCal is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trabalhos_TransCal.  If not, see <https://www.gnu.org/licenses/>.
%}

%% Calcula as areas de um conjunto de triangulos
% T eh um vetor com os indices dos vertices dos triangulos
% P eh o vetor de coordenadas 2D indexado por T
% A eh um vetor contendo as areas de cada triangulo
function A = area_from_T(T, P)
    % T(:, t) e um vetor contendo todos os vertices do triangulo t
    % P(:, p) e o vetor de coordenadas do ponto p
    % P(:, T(:,t)) é o vetor de coordenadas de todos os vértices do triangulo t
    A = abs(P(1, T(1, :)) .* ( P(2, T(2, :)) - P(2, T(3, :)) )...
          + P(1, T(2, :)) .* ( P(2, T(3, :)) - P(2, T(1, :)) )...
          + P(1, T(3, :)) .* ( P(2, T(1, :)) - P(2, T(2, :)) )) / 2;
end
