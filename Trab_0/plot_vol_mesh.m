%{
Copyright 2018 Luiz Georg

This file is part of Trabalhos_TransCal.

    Trabalhos_TransCal is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Trabalhos_TransCal is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Trabalhos_TransCal.  If not, see <https://www.gnu.org/licenses/>.
%}

function plot_vol_mesh(P, E, T)

    pdemesh(P, E, T);
    hold on;
    Pmed12 = ponto_medio(P(:,T(1,:)), P(:,T(2,:)));
    Pmed23 = ponto_medio(P(:,T(2,:)), P(:,T(3,:)));
    Pmed31 = ponto_medio(P(:,T(3,:)), P(:,T(1,:)));
    P0 = ponto_medio(P(:,T(1,:)), P(:,T(2,:)), P(:,T(3,:)));

    k = length(P0);

    for n = 1:k
        plot([P0(1,n), Pmed12(1,n)], [P0(2,n), Pmed12(2,n)], 'g');
        plot([P0(1,n), Pmed23(1,n)], [P0(2,n), Pmed23(2,n)], 'g');
        plot([P0(1,n), Pmed31(1,n)], [P0(2,n), Pmed31(2,n)], 'g');
    end
end
