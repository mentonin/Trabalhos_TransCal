# Trabalhos Transcal

Esse repositório contém os trabalhos da disciplina de Transferência de Calor, da Universidade de Braśılia, ćodigo 168033, Turma AA, do segundo semestre de 2018. Todo o código fonte está protegido pela licensa GPLv3 ou, à sua preferência, uma versão mais nova, e é um Copyright de Luiz Georg.

## MatLab/Octave

Esse repositório foi desenvolvido em Octave, mas feito para ser facilmente adaptável para MatLab. Para poder utilizar o código em MATLAB, você deve comentar as linhas marcadas com `% OCTAVE`, e descomentar linhas marcadas com `% MATLAB`.
